package by.epam.dao;

import by.epam.configuration.TestConfiguration;
import by.epam.model.Item;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@Transactional
public class ItemDAOTest {
    @Autowired
    private ItemDAO dao;

    @Test
    public void readAllItems() {
        final int expected = 1000;
        List<Item> items = dao.findAll();
        final int actual = items.size();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addNewItem() {
        Item test = Item.builder().name("test item").price((new BigDecimal(99))).build();
        dao.save(test);
        boolean isExist = dao.existsById(test.getId());
        Assert.assertTrue(isExist);
    }

    @Test
    public void deleteItem() {
        Item test = Item.builder().id(1).build();
        dao.deleteById(test.getId());
        boolean isExist = dao.existsById(test.getId());
        Assert.assertFalse(isExist);
    }

    @Test
    public void updateItem(){
        Item test = Item.builder().id(2).name("test").price(new BigDecimal(100)).build();
        dao.save(test);
        Item actual = dao.getOne(test.getId());
        Assert.assertEquals(test, actual);
    }
}