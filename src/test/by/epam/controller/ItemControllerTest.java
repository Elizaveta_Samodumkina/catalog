package by.epam.controller;

import by.epam.configuration.TestConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@org.springframework.test.context.web.WebAppConfiguration
public class ItemControllerTest {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void initialize() {
        ServletContext servletContext = wac.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("itemController"));
    }

    @Test
    public void takeItem() throws Exception {
        this.mockMvc.perform(get("/items/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
        //.andExpect(jsonPath("$.name").value("Lee"));
    }

    @Test
    public void takeNotExistItem() throws Exception {
        this.mockMvc.perform(get("/items/100000000"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    @Test
    public void takeItems() throws Exception {
        this.mockMvc.perform(get("/items"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void takeItemsInSpecificCurrency() throws Exception {
        this.mockMvc.perform(get("/USD/items"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void takeItemsInNotExistCurrency() throws Exception {
        this.mockMvc.perform(get("/a/items"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void takeItemInSpecificCurrency() throws Exception {
        this.mockMvc.perform(get("/USD/items/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void addItem() throws Exception {
        this.mockMvc.perform(post("/items").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{  \"name\": \"test\",\n" +
                        "  \"price\": 0\n" +
                        "}"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void tryFindPageWithNotAllowedMethodHere() throws Exception {
        this.mockMvc.perform(post("/items/1").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{  \"name\": \"test\",\n" +
                        "  \"price\": 0\n" +
                        "}"))
                .andExpect(status().isMethodNotAllowed())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateItem() throws Exception {
        this.mockMvc.perform(put("/items").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"id\": 5,\n" +
                        "  \"name\": \"test\",\n" +
                        "  \"price\": 10\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateNotExistItem() throws Exception {
        this.mockMvc.perform(put("/items").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"id\": 1000000,\n" +
                        "  \"name\": \"test\",\n" +
                        "  \"price\": 10\n" +
                        "}"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteItem() throws Exception {
        this.mockMvc.perform(delete("/items/id").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}