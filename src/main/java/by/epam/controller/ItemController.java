package by.epam.controller;

import by.epam.dto.ItemDTO;
import by.epam.model.Item;
import by.epam.service.ItemService;
import by.epam.service.exception.DoesNotExistItemException;
import by.epam.service.exception.InvalidCurrencyCodeException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemController {
    private final static String ERROR_KEY = "error";
    private final static String SAVED_VALUE_KEY = "saved_value";
    private final static String SUCCESS_KEY = "success";
    private final static String MESSAGE_KEY = "message";

    @Autowired
    private ItemService service;

    @RequestMapping(value = "/{currency}/items", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity takeAllItemsInSpecificCurrency(@PathVariable("currency") String currencyName) {
        ResponseEntity result = null;
        try {
            List<ItemDTO> items = service.findAllInSpecificCurrency(currencyName);
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(items);
        } catch (InvalidCurrencyCodeException e) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body((new JSONObject().accumulate(ERROR_KEY, e.getMessage())).toString());
        }
        return result;
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ItemDTO> takeAllItems() {
        List<ItemDTO> items = service.findAll();
        return items;
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity takeAllItems(@PathVariable("id") int id) {
        ResponseEntity result = null;
        try {
            ItemDTO item = service.findById(id);
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(item);
        } catch (DoesNotExistItemException e) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body((new JSONObject().accumulate(ERROR_KEY, e.getMessage())).toString());
        }
        return result;
    }

    @RequestMapping(value = "/{currency}/items/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity takeAllItems(@PathVariable("currency") String currencyName, @PathVariable("id") int id) {
        ResponseEntity result = null;
        try {
            ItemDTO item = service.findByIdInSpecificCurrency(id, currencyName);
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(item);
        } catch (DoesNotExistItemException | InvalidCurrencyCodeException e) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body((new JSONObject().accumulate(ERROR_KEY, e.getMessage())).toString());
        }
        return result;
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public ResponseEntity addItem(@RequestBody ItemDTO itemDTO) {
        ItemDTO item = service.add(itemDTO);
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                .body((new JSONObject().accumulate(SAVED_VALUE_KEY, item).toString()));
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.POST)
    public ResponseEntity addItem(@RequestBody ItemDTO itemDTO, @PathVariable("id") int id) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).contentType(MediaType.APPLICATION_JSON)
                .body((new JSONObject().accumulate(ERROR_KEY, "method is not allowed").toString()));
    }

    @RequestMapping(value = "/items", method = RequestMethod.PUT)
    public ResponseEntity updateItem(@RequestBody Item item) {
        ResponseEntity result = null;
        try {
            service.update(item);
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, true).toString());
        } catch (DoesNotExistItemException e) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, false).
                            accumulate(MESSAGE_KEY, e.getMessage()).toString());
        }
        return result;
    }

    @RequestMapping(value = "/items", method = RequestMethod.DELETE)
    public ResponseEntity delete() {
        service.delete();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(new JSONObject().accumulate(SUCCESS_KEY, true).toString());
    }

    @RequestMapping(value = "/items/name", method = RequestMethod.DELETE)
    public ResponseEntity deleteItem(@RequestBody String name) {
        ResponseEntity result = null;
        try {
            service.delete(ItemDTO.builder().name(name).build());
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, true).toString());
        } catch (DoesNotExistItemException doesNotExistItem) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, false).
                            accumulate(MESSAGE_KEY, doesNotExistItem.getMessage()).toString());
        }
        return result;
    }

    @RequestMapping(value = "/items/id", method = RequestMethod.DELETE)
    public ResponseEntity deleteItem(@RequestBody int id) {
        ResponseEntity result = null;
        try {
            service.delete(id);
            result = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, true).toString());
        } catch (DoesNotExistItemException doesNotExistItem) {
            result = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON)
                    .body(new JSONObject().accumulate(SUCCESS_KEY, false).
                            accumulate(MESSAGE_KEY, doesNotExistItem.getMessage()).toString());
        }
        return result;
    }

}
