package by.epam.dao;

import by.epam.model.CurrencyRates;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRatesDAO extends JpaRepository<CurrencyRates, Integer> {
}
