package by.epam.dto;

import by.epam.model.Item;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Converter {
    @Autowired
    private ModelMapper modelMapper;

    public Item convertDTOToItem(ItemDTO itemDTO) {
        Item item = modelMapper.map(itemDTO, Item.class);
        return item;
    }

    public ItemDTO convertItemToDTO(Item item){
        ItemDTO itemDTO = modelMapper.map(item, ItemDTO.class);
        return itemDTO;
    }

    public List<ItemDTO> convertListItemToListDTO(List<Item> items){
        List<ItemDTO> result = new ArrayList<>();

        for (Item item: items){
            result.add(convertItemToDTO(item));
        }

        return result;
    }

    public List<Item> convertListDTOToListItem(List<ItemDTO> itemDTOs){
        List<Item> result = new ArrayList<>();

        for (ItemDTO item: itemDTOs){
            result.add(convertDTOToItem(item));
        }

        return result;
    }
}
