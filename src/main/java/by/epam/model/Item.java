package by.epam.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "item", schema = "public")

@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data
class Item implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "item_id_seq")
    @SequenceGenerator(name="item_id_seq", sequenceName="item_id_seq", allocationSize=1)
    private int id;
    @NotBlank
    private String name;
    @NotNull
    private BigDecimal price;
}
