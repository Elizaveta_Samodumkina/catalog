package by.epam.model;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "currency_rates", schema = "public")

@NoArgsConstructor
@AllArgsConstructor
@Builder
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public @Data
class CurrencyRates {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_rates_id_seq")
    @SequenceGenerator(name = "currency_rates_id_seq", sequenceName = "currency_rates_id_seq", allocationSize = 1)
    private int id;
    private LocalDate date;
    @Type(type = "jsonb")
    @Column(name = "rates", columnDefinition = "jsonb")
    private String ratesJSON;


}
