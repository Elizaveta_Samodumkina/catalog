package by.epam.service.externaldata;

import by.epam.service.exception.InvalidCurrencyCodeException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Component
public class CurrencyRateFromFixer {
    private final static String SUCCESS_KEY = "success";
    private final static String RATE_KEY = "rates";
    private final static String DATE_KEY = "date";

    private final static String SYMBOLS = "&symbols=";

    private final static String URI = "http://data.fixer.io/api/latest?access_key=4281e5abcabd8097875afbed2c220a7e&format=1";

    public JSONObject takeCurrencyRate(String currencyName) {
        StringBuilder builder = new StringBuilder(URI + SYMBOLS + currencyName);
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(builder.toString(), String.class);
        return new JSONObject(result);
    }

    public JSONObject takeCurrencyRate() {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(URI, String.class);
        return new JSONObject(result);
    }

    public boolean isJSONWithSuccess(JSONObject json) {
        return json.getBoolean(SUCCESS_KEY);
    }

    public double takeRateFromJSON(JSONObject json, String currencyName) {
        JSONObject rates = (JSONObject) json.get(RATE_KEY);
        return rates.getDouble(currencyName);
    }

    public JSONObject takeRatesFromJSON(JSONObject json) {
        return (JSONObject) json.get(RATE_KEY);
    }

    public LocalDate takeDateFromJSON(JSONObject json) {
        String temp = json.getString(DATE_KEY);
        return LocalDate.parse(temp);
    }

    public double takeCurrencyRateValue(JSONObject json, String currencyName) throws InvalidCurrencyCodeException {
        if (!isJSONWithSuccess(json)) {
            throw new InvalidCurrencyCodeException("unknown currency code " + currencyName);
        } else {
            return takeRateFromJSON(json, currencyName);
        }
    }
}
