package by.epam.service;

import by.epam.dto.ItemDTO;
import by.epam.model.Item;
import by.epam.service.exception.DoesNotExistItemException;
import by.epam.service.exception.InvalidCurrencyCodeException;

import java.util.List;

public interface ItemService {
    public ItemDTO add(ItemDTO item);
    public void update(Item item) throws DoesNotExistItemException;
    public void delete();
    public void delete(int id) throws DoesNotExistItemException;
    public void delete(ItemDTO item) throws DoesNotExistItemException;
    public List<ItemDTO> findAll();
    public ItemDTO findById(int id) throws DoesNotExistItemException;
    public ItemDTO findByIdInSpecificCurrency(int id, String currencyName) throws DoesNotExistItemException,
            InvalidCurrencyCodeException;
    public List<ItemDTO> findAllInSpecificCurrency(String currencyName) throws InvalidCurrencyCodeException;
}
