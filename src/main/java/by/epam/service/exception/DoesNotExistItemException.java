package by.epam.service.exception;

public class DoesNotExistItemException extends Exception {
    public DoesNotExistItemException() {
    }

    public DoesNotExistItemException(String message) {
        super(message);
    }

    public DoesNotExistItemException(String message, Exception e) {
        super(message, e);
    }

    public DoesNotExistItemException(Exception e) {
        super(e);
    }
}
