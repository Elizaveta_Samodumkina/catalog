package by.epam.service.exception;

public class InvalidCurrencyCodeException extends Exception{
    public InvalidCurrencyCodeException() {
    }

    public InvalidCurrencyCodeException(String message) {
        super(message);
    }

    public InvalidCurrencyCodeException(String message, Exception e) {
        super(message, e);
    }

    public InvalidCurrencyCodeException(Exception e) {
        super(e);
    }
}
