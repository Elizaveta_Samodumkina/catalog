package by.epam.service;

public interface CurrencyRateService extends Runnable {
    public void takeRate() throws InterruptedException;
}
