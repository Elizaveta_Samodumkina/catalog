package by.epam.service.impl;

import by.epam.dao.ItemDAO;
import by.epam.dto.Converter;
import by.epam.dto.ItemDTO;
import by.epam.model.Item;
import by.epam.service.ItemService;
import by.epam.service.exception.DoesNotExistItemException;
import by.epam.service.exception.InvalidCurrencyCodeException;
import by.epam.service.externaldata.CurrencyRateFromFixer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    private final static int FIRST = 0;
    private final static int EMPTY = 0;

    @Autowired
    private ItemDAO dao;
    @Autowired
    private CurrencyRateFromFixer currencyRates;
    @Autowired
    private Converter converter;

    @Override
    @Transactional
    public ItemDTO add(ItemDTO item) {
        Item temp = dao.save(converter.convertDTOToItem(item));
        return converter.convertItemToDTO(temp);
    }

    @Override
    public void update(Item item) throws DoesNotExistItemException {
        if (dao.existsById(item.getId())){
            dao.deleteById(item.getId());
            dao.save(item);
        }else{
            throw new DoesNotExistItemException("item with id " + item.getId() + " does not exist");
        }
    }

    @Override
    public void delete() {
        dao.deleteAll();
    }

    @Override
    public void delete(int id) throws DoesNotExistItemException {
        if (dao.existsById(id)) {
            dao.deleteById(id);
        }else{
            throw new DoesNotExistItemException("item with id " + id + " does not exist");
        }
    }

    @Override
    public void delete(ItemDTO item) throws DoesNotExistItemException {
        List<Item> temp = dao.findByName(item.getName());
        if (temp.size() == EMPTY){
            throw new DoesNotExistItemException("item with name " + item.getName() + " does not exist");
        }else{
            dao.delete(temp.get(FIRST));
        }
    }

    @Override
    public List<ItemDTO> findAll() {
        return converter.convertListItemToListDTO(dao.findAll());
    }

    @Override
    public ItemDTO findById(int id) throws DoesNotExistItemException {
        if (dao.existsById(id)) {
            return converter.convertItemToDTO(dao.findById(id).get());
        }else{
            throw new DoesNotExistItemException("item with id " + id + " does not exist");
        }
    }

    @Override
    public ItemDTO findByIdInSpecificCurrency(int id, String currencyName) throws DoesNotExistItemException, InvalidCurrencyCodeException {
        ItemDTO item = findById(id);
        double value = takeCurrencyRateValue(currencyName);
        Item temp = converter.convertDTOToItem(item);
        recalculateItemPriceWithCurrencyRate(temp, value);
        return converter.convertItemToDTO(temp);
    }

    @Override
    public List<ItemDTO> findAllInSpecificCurrency(String currencyName) throws InvalidCurrencyCodeException {

        double value = takeCurrencyRateValue(currencyName);

        List<Item> items = dao.findAll();
        recalculateItemsPriceWithCurrencyRate(items, value);
        return converter.convertListItemToListDTO(items);
    }

    private double takeCurrencyRateValue(String currencyName) throws InvalidCurrencyCodeException {
        JSONObject json = currencyRates.takeCurrencyRate(currencyName);
        return currencyRates.takeCurrencyRateValue(json, currencyName);
    }

    private void recalculateItemsPriceWithCurrencyRate(List<Item> items, double value) {
        for (Item item : items) {
            recalculateItemPriceWithCurrencyRate(item, value);
        }
    }

    private void recalculateItemPriceWithCurrencyRate(Item item, double value){
        BigDecimal temp = (new BigDecimal(item.getPrice().multiply(new BigDecimal(value)).doubleValue()))
                .setScale(2, RoundingMode.HALF_UP);
        item.setPrice(temp);
    }
}
