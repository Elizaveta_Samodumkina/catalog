package by.epam.service.impl;

import by.epam.dao.CurrencyRatesDAO;
import by.epam.model.CurrencyRates;
import by.epam.service.CurrencyRateService;
import by.epam.service.externaldata.CurrencyRateFromFixer;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class CurrencyRateServiceImpl implements CurrencyRateService {
    @Autowired
    private CurrencyRateFromFixer currencyRateFromFixer;
    @Autowired
    private CurrencyRatesDAO dao;

    @Override
    public void takeRate() throws InterruptedException {
        while (true) {
            JSONObject json = currencyRateFromFixer.takeCurrencyRate();
            if (!currencyRateFromFixer.isJSONWithSuccess(json)) {
                log.debug("cannot take rates");
                TimeUnit.MINUTES.sleep(10);
            } else {
                writeToBD(json);
                break;
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                takeRate();
                TimeUnit.HOURS.sleep(24);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    private void writeToBD(JSONObject jsonObject) {
        LocalDate date = currencyRateFromFixer.takeDateFromJSON(jsonObject);
        JSONObject rates = currencyRateFromFixer.takeRatesFromJSON(jsonObject);
        CurrencyRates currencyRates = CurrencyRates.builder().date(date).ratesJSON(rates.toString()).build();

        dao.save(currencyRates);
    }
}
