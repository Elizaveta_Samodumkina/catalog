package by.epam.configuration.listener;

import by.epam.service.impl.CurrencyRateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ReadDailyCurrencyRates implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private CurrencyRateServiceImpl service;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Thread thread = new Thread(service);
        thread.setDaemon(true);
        thread.start();
    }
}
