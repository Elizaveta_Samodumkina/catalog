create table public."item"(
  id serial primary key,
  name  CHARACTER VARYING(50) not null,
  price numeric(6,4) not null
);